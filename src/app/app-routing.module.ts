import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { BlogdetailsComponent } from './components/blogdetails/blogdetails.component';
import { BloglistComponent } from './components/bloglist/bloglist.component';

const routes: Routes = [
  {path:'', component: BloglistComponent},
  {path:'blogdetails' , component:BlogdetailsComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
