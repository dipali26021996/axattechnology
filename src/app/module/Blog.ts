export class Blog{
    id :number;
    title: string;
    blog_url: string;
    content: string;
    image: string;
    author: string;
    imgUrl: string;
    category_id: string;
    meta_title: string;
    meta_description: string;
    adv_title: string;
    adv_description: string;
    adv_image: string;
    adv_buttonlink: string;
    adv_butttonname: string;
   created_at: string;
    updated_at: string;


    constructor(){
        this.id=0;
        this.title="";
        this.blog_url="";
        this.content="";
        this.image="";
        this.author="";
        this.imgUrl="";
        this.category_id="";
        this.meta_title="";
        this.meta_description="";
        this.adv_title="";
        this.adv_description="";
        this.adv_image="";
        this.adv_butttonname="";
        this.created_at="";
        this.updated_at="";
        this.adv_buttonlink="";




    }
}