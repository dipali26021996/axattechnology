import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root',
})
export class BlogApiService {
  constructor(private httpClient: HttpClient) {}

  public getBlogListCallApi() {
  return this.httpClient.get("https://docully.com/blog/api/blogs");
}

}