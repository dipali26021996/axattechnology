import { Component, OnInit } from '@angular/core';
import { Blog } from 'src/app/module/Blog';
import { BlogApiService } from 'src/app/service/BlogApiService';

@Component({
  selector: 'app-blogdetails',
  templateUrl: './blogdetails.component.html',
  styleUrls: ['./blogdetails.component.css']
})
export class BlogdetailsComponent implements OnInit {
  blog: Blog;
  blogs: Blog[];
  tempBlog : any
  constructor(private  blogApiService: BlogApiService) {
    this.blog= new Blog();
    this.blogs =[];
   }

   getData(){
     this.blogApiService.getBlogListCallApi().subscribe(
       (output:any)=>{
         this.blogs = JSON.parse(JSON.stringify(output.blogs));
         console.log(this.blogs);
         
       })
   }


  ngOnInit(): void {
    this.tempBlog=localStorage.getItem('blog');
    this.blog=JSON.parse(this.tempBlog)
    console.log(this.blog.id);
    this.getData();
  }

}
