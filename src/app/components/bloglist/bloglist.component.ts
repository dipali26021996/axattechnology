import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Blog } from 'src/app/module/Blog';
import { BlogApiService } from 'src/app/service/BlogApiService';

@Component({
  selector: 'app-bloglist',
  templateUrl: './bloglist.component.html',
  styleUrls: ['./bloglist.component.css']
})
export class BloglistComponent implements OnInit {
  blogs: Blog[];
  blog:Blog;
  constructor(private blogApiService : BlogApiService, private router: Router) {
    this.blog =new Blog();
    this.blogs =[];

   }

  
  getData(){
    this.blogApiService.getBlogListCallApi().subscribe(
      (output:any)=>{
        this.blogs = JSON.parse(JSON.stringify(output.blogs));
      })
  }

  getBlogDetails(blog:any){
  blog=JSON.stringify(blog);
    localStorage.setItem("blog",blog);
    this.router.navigateByUrl('/blogdetails');
  }
  ngOnInit(): void {
    this.getData();
  }

}
